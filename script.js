"use strict";

let numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?");
let countMovie = 0;

const personalMovieDB = {
    count: numberOfFilms,
    movies: { },
    actors: { },
    genres: [],
    privat: false,
};

if (personalMovieDB.count < 10) {
    alert("Просмотрено довольно мало фильмов");
} else if (personalMovieDB.count <= 30) {
        alert("Вы классический зритель");
} else if (personalMovieDB.count > 30) {
        alert("Вы киноман");
} else {
        alert("Произошла ошибка");
}

let control=Number(numberOfFilms);

for (let i = 0; i < control; i++){
    const a = prompt("Один из последних просмотренных фильмов?");
    const b = prompt("На сколько оцените его?");
    if ((a === null) || (b === null) || (a == '') || (b == '') || (a.length > 50)){
        i--;
        console.log("Error");
    } else {
        personalMovieDB.movies[a] = b;
        console.log("Ok");
    }
}

while (countMovie < control){
    const a = prompt("Один из последних просмотренных фильмов?");
    const b = prompt("На сколько оцените его?");
    if ((a !== null) && (b !== null) && (a != '') && (b != '') && (a.length <= 50)){
        personalMovieDB.movies[a] = b;
        countMovie++;
    }
}

countMovie = 0;

do{
    const a = prompt('Один из последних просмотренных фильмов?', '');
    const b = prompt('На сколько оцените его?', '');
    if ((a !== null) && (b !== null) && (a != '') && (b != '') && (a.length <= 50)){
        personalMovieDB.movies[a] = b;
        countMovie++;
    }
} while (countMovie < control);